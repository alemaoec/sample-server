# sample-server

A barebones Node.js app using [Express](http://expressjs.com/).

## Rodando localmente

Tenha certeza que você tenha o NodeJs instalado [Node.js](http://nodejs.org/).

```sh
git clone git@gitlab.com:alemaoec/sample-server.git #
cd sample-server
npm install
npm start
```

A sua aplicação deverá estar rodando em [localhost:5000](http://localhost:5000/).